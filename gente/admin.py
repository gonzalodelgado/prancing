import json
from datetime import date, datetime, time, timedelta
from django.contrib import admin
from django.template.defaultfilters import timesince
from django.shortcuts import get_object_or_404
from django.conf.urls import patterns, url
from django.http import HttpResponse
from reversion import VersionAdmin

from gente.models import (
    Cargo, Empleado, Cliente, Huesped, Proveedor, RegistroDeActividad)
from gente.forms import EmpleadoAdminForm, ClienteAdminForm


class EmpleadoAdmin(VersionAdmin):
    form = EmpleadoAdminForm
    list_display = 'dni', 'apellido', 'nombre', 'cantidad_de_hijos'
    search_fields = 'user__last_name', 'user__first_name', 'cargo', 'user__username'
    fieldsets = (
        (u'Datos personales', {
            'fields': ('dni', ('apellido', 'nombre'), 'cantidad_de_hijos')}),
        (u'Datos de contacto', {
            'fields': ('domicilio', ('localidad', 'provincia'), ('telefono', 'email'))}),
        (u'Datos laborales', {'fields': ('cuil', 'fecha_ingreso', 'cargo')}))

    def ajax(self, request, empleado_id):
        empleado = get_object_or_404(self.model, pk=empleado_id)
        datos = {
            'hijos': empleado.cantidad_de_hijos,
            'apellido': empleado.apellido,
            'nombre': empleado.nombre,
            'antiguedad': empleado.antiguedad,
            'fechaIngreso': datetime.combine(
                empleado.fecha_ingreso, time()
            ).isoformat(),
        }
        if empleado.cargo:
            datos['cargo'] = {
                'puesto': empleado.cargo.puesto,
                'sueldo': float(empleado.cargo.sueldo),
                'turno': empleado.cargo.turno,
            }
        return HttpResponse(json.dumps(datos), content_type='application/json')

    def get_urls(self):
        urls = patterns('',
            url(r'^(?P<empleado_id>\d+)/ajax/$',
                self.admin_site.admin_view(self.ajax),
                name='datos-empleado'),
        )
        return urls + super(EmpleadoAdmin, self).get_urls()

admin.site.register(Empleado, EmpleadoAdmin)


class ClienteAdmin(VersionAdmin):
    form = ClienteAdminForm

admin.site.register(Cliente, ClienteAdmin)


class RegistroDeActividadAdmin(admin.ModelAdmin):
    list_display = 'fecha_y_hora', 'empleado', 'actividad'
    search_fields = ('user__first_name', 'user__last_name',
                     'user__username', 'change_message', 'object_repr')
    readonly_fields = ('action_time', 'user', 'content_type', 'object_id',
                       'object_repr', 'action_flag', 'change_message')

    def fecha_y_hora(self, obj):
        ayer = datetime.now() - timedelta(days=1)
        action_time = obj.action_time
        if obj.action_time < ayer:
            return date(obj.action_time, 'j \d\e F \d\e Y, H:i')
        return u'Hace %s' % timesince(obj.action_time)

    def empleado(self, obj):
        usuario = obj.user
        if usuario.empleado_set.exists():
            empleado = usuario.empleado_set.all()[0]
            return u'%s %s' % (empleado.apellido, empleado.nombre)
        return u'%s %s' % (usuario.last_name, usuario.first_name)

    def actividad(self, obj):
        return unicode(obj)

admin.site.register(RegistroDeActividad, RegistroDeActividadAdmin)


admin.site.register(Huesped)
admin.site.register(Proveedor)
admin.site.register(Cargo)
