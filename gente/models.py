# -*- coding: utf-8 -*-
from datetime import date

from django.db import models
from django.contrib.auth.models import User
from django.contrib.localflavor.ar.forms import PROVINCE_CHOICES
from django.contrib.admin.models import LogEntry


class PersonaBase(models.Model):
    dni = models.CharField(max_length=8)
    domicilio = models.CharField(max_length=200)
    localidad = models.CharField(max_length=200)
    provincia = models.CharField(max_length=50, choices=PROVINCE_CHOICES)
    telefono = models.CharField(u'Teléfono', max_length=16, blank=True)

    class Meta:
        abstract = True


class PersonaUsuario(PersonaBase):
    user = models.ForeignKey(User, editable=False)

    class Meta(PersonaBase.Meta):
        abstract = True

    @property
    def apellido(self):
        try:
            return self.user.last_name
        except User.DoesNotExist:
            return None

    @apellido.setter
    def apellido(self, valor):
        try:
            self.user.last_name = valor
        except User.DoesNotExist:
            self.user = User(last_name=valor)

    @property
    def nombre(self):
        try:
            return self.user.first_name
        except User.DoesNotExist:
            return None

    @nombre.setter
    def nombre(self, valor):
        try:
            self.user.first_name = valor
        except User.DoesNotExist:
            self.user = User(first_name=valor)

    @property
    def email(self):
        try:
            return self.user.email
        except User.DoesNotExist:
            return None

    @email.setter
    def email(self, valor):
        try:
            self.user.email = valor
        except User.DoesNotExist:
            self.user = User(email=valor)


class PersonaAutonomo(PersonaBase):
    apellido = models.CharField(max_length=50)
    nombre = models.CharField(max_length=50)
    email = models.EmailField(blank=True, null=True)

    class Meta(PersonaBase.Meta):
        abstract = True


OPCIONES_CARGO = (
        ('cocinero', 'Cocinero'),
        ('encargado', 'Encargado'),
        ('mozo', 'Mozo'),
        ('mucama', 'Mucama'),
)


class Cargo(models.Model):
    puesto = models.CharField(max_length=200)
    turno = models.CharField(max_length=30, choices=(
        (u'mañana', u'Mañana'), ('tarde', 'Tarde'), ('noche', 'Noche')))
    hora_ingreso = models.TimeField()
    hora_salida = models.TimeField()
    sueldo = models.DecimalField(max_digits=15, decimal_places=2)

    def __unicode__(self):
        return u'%s (%s)' % (self.puesto, self.get_turno_display())


class Empleado(PersonaUsuario):
    cuil = models.CharField(max_length=15, unique=True, null=True)
    fecha_ingreso = models.DateField()
    cantidad_de_hijos = models.PositiveSmallIntegerField(default=0)
    cargo = models.ForeignKey(Cargo, null=True, related_name='empleados')

    def __unicode__(self):
        return u'%s, %s (empleado #%d)' % (self.apellido, self.nombre, self.id)

    @property
    def antiguedad(self):
        hoy = date.today()
        return (hoy - self.fecha_ingreso).days / 365

class Cliente(PersonaAutonomo):
    tipo_cliente = models.CharField('Tipo de cliente', max_length=20, default='particular', choices=(
        ('particular', 'Particular'), ('empresa', 'Empresa')))
    cuit_o_cuil = models.CharField(max_length=15, db_index=True)

    def __unicode__(self):
        return u'Cliente %s, %s' % (self.apellido, self.nombre)


class Huesped(PersonaAutonomo):

    class Meta:
        verbose_name_plural = u'Huespedes'

    def __unicode__(self):
        return u'Huesped %s, %s' % (self.apellido, self.nombre)


class Proveedor(PersonaAutonomo):
    cuit = models.CharField(max_length=15, unique=True, db_index=True)

    class Meta:
        verbose_name_plural = u'Proveedores'

    def __unicode__(self):
        return u'%s - %s %s' % (self.cuit, self.apellido, self.nombre)


class RegistroDeActividad(LogEntry):
    class Meta:
        proxy = True
        verbose_name_plural = u'Registros de actividad'
