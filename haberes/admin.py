import json
from datetime import date

from django.contrib import admin
from django.http import HttpResponse
from django.conf.urls import patterns
from django.shortcuts import get_object_or_404

from .models import Liquidacion, Concepto, LiquidacionDetalle
from .forms import LiquidacionAdminForm


class DetalleInline(admin.TabularInline):
    model = LiquidacionDetalle


class LiquidacionAdmin(admin.ModelAdmin):
    readonly_fields = 'id',
    inlines = [DetalleInline]
    form = LiquidacionAdminForm
    exclude = 'usuario',
    fieldsets = (
        (None, {
            'fields': (
                ('id', 'anio', 'mes', 'tipo'),
                ('fecha_pago', 'notas'), 'empleado'
            )
        }),
    )

    def save_model(self, request, obj, form, change):
        obj.usuario = request.user
        obj.periodo = date(
            form.cleaned_data['anio'], form.cleaned_data['mes'], 1)
        obj.total_remunerativo = obj.total()
        obj.save()

admin.site.register(Liquidacion, LiquidacionAdmin)



class ConceptoAdmin(admin.ModelAdmin):
    def ajax(self, request, concepto_id):
        concepto = get_object_or_404(self.model, pk=concepto_id)
        datos = {
            'tipo': concepto.tipo,
            'deduccion': concepto.deduccion,
            'monto': float(concepto.monto),
        }
        return HttpResponse(json.dumps(datos), content_type='application/json')

    def get_urls(self):
        urls = patterns('',
            (r'^(?P<concepto_id>\d+)/ajax/$',
             self.admin_site.admin_view(self.ajax)),
        )
        return urls + super(ConceptoAdmin, self).get_urls()


admin.site.register(Concepto, ConceptoAdmin)
