# coding: utf-8
from datetime import date
from django import forms

from .models import Liquidacion


def generar_opciones_anio():
    hoy = date.today()
    return [(a, a) for a in range(hoy.year - 10, hoy. year + 10)]


class LiquidacionAdminForm(forms.ModelForm):
    anio = forms.TypedChoiceField(
        label=u'Año', choices=generar_opciones_anio(), coerce=int)
    mes = forms.TypedChoiceField(
        label='Mes', choices=[(m, m) for m in range(1, 13)], coerce=int)

    class Meta:
        model = Liquidacion
