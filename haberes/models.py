# -*- coding: utf-8 -*-
from decimal import Decimal

from django.db import models


LIQ_ESPECIAL = 'especial'
LIQ_MENSUAL = 'mensual'
TIPOS_LIQUIDACION = LIQ_MENSUAL, LIQ_ESPECIAL
class Liquidacion(models.Model):
    usuario = models.ForeignKey('auth.User', null=True)
    tipo = models.CharField(
        max_length=20, choices=[(t, t) for t in TIPOS_LIQUIDACION])
    periodo = models.DateField(u'Período')
    notas = models.TextField(blank=True)
    total_remunerativo = models.DecimalField(max_digits=15, decimal_places=2)
    empleado = models.ForeignKey('gente.Empleado')
    fecha_pago = models.DateField(null=True, blank=True)

    class Meta:
        verbose_name_plural = u'Liquidaciones'

    def __unicode__(self):
        return '%s %s' % (self.periodo, self.empleado)

    def total_descuentos(self):
        return sum(self.liquidaciondetalle_set.filter(
            concepto__deduccion=True).values_list('importe', flat=True))

    def total_remunerativos(self):
        return sum(self.liquidaciondetalle_set.filter(
            concepto__deduccion=False,
        ).exclude(concepto__tipo='AUH').values_list('importe', flat=True))

    def total_no_remunerativos(self):
        return sum(self.liquidaciondetalle_set.filter(
            concepto__deduccion=False, concepto__tipo='AUH',
        ).values_list('importe', flat=True))

    @property
    def antiguedad_empleado(self):
        return self.empleado.antiguedad

    @property
    def total_antiguedad(self):
        sueldo = self.empleado.cargo.sueldo
        return self.empleado.antiguedad * Decimal('0.01') * sueldo

    def total(self):

        detalles = sum(self.liquidaciondetalle_set.values_list('importe', flat=True))
        return self.basico + self.total_antiguedad + detalles

    @property
    def basico(self):
        sueldo = self.empleado.cargo.sueldo
        return sueldo / 2 if self.tipo == LIQ_ESPECIAL else sueldo


CONCEPTO_FIJO = 'fijo'
CONCEPTO_PORCENTUAL = 'porcentual'
CONCEPTO_AUH = 'AUH'
TIPOS_CONCEPTO = CONCEPTO_FIJO, CONCEPTO_PORCENTUAL, CONCEPTO_AUH

class Concepto(models.Model):
    tipo = models.CharField(
        max_length=50, choices=[(t, t) for t in TIPOS_CONCEPTO])
    descripcion = models.TextField()
    deduccion = models.BooleanField(u'Deducción')
    monto = models.DecimalField(max_digits=15, decimal_places=2)

    def __unicode__(self):
        nombre = self.descripcion
        if self.deduccion:
            nombre = u'%s (deducción)' % nombre
        return nombre

    def save(self, *args, **kwargs):
        r = super(Concepto, self).save(*args, **kwargs)
        for detalle in self.liquidaciondetalle_set.all():
            detalle.save()
        return r


class LiquidacionDetalle(models.Model):
    liquidacion = models.ForeignKey(Liquidacion)
    concepto = models.ForeignKey(Concepto)
    importe = models.DecimalField(max_digits=15, decimal_places=2)

    class Meta:
        verbose_name = u'Detalle de Liquidación'
        verbose_name_plural = u'Detalles de Liquidación'

    def __unicode__(self):
        return u'%s: %s' % (self.concepto, self.importe)

    def save(self, *args, **kwargs):
        if not self.concepto.deduccion and self.importe < 0:
            self.importe *= -1
        if self.concepto.deduccion and self.importe > 0:
            self.importe *= -1
        return super(LiquidacionDetalle, self).save(*args, **kwargs)
