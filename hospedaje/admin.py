from django.contrib import admin

from prancing.admin import AutoUserFieldAdmin
from hospedaje.models import (
    Habitacion, Alquiler, Modalidad, Reserva, Categoria, PagoReserva, PagoAlquiler)


class PagoAlquilerInline(admin.TabularInline):
    model = PagoAlquiler
    exclude = 'concepto',


class AlquilerAdmin(AutoUserFieldAdmin):
    raw_id_fields = 'cliente',
    user_field = 'aprobado_por'
    inlines = [PagoAlquilerInline]
    filter_horizontal = 'habitaciones',
    list_display = 'cliente', 'fecha_ingreso', 'fecha_egreso', 'modalidad', 'monto'
    fieldsets = (
        (None, {'fields': ['aprobado_por']}),
        (None, {'fields': ('reserva', 'cliente',
            ('fecha_ingreso', 'hora_ingreso'),
            ('fecha_egreso', 'hora_egreso'))}),
        (None, {'fields': ('habitaciones', 'modalidad', 'observaciones')}))

admin.site.register(Alquiler, AlquilerAdmin)


class PagoReservaInline(admin.TabularInline):
    model = PagoReserva
    exclude = 'concepto',


class ReservaAdmin(AlquilerAdmin):
    raw_id_fields = 'responsable',
    user_field = 'solicitado_a'
    inlines = [PagoReservaInline]
    filter_horizontal = 'habitaciones',
    list_display = 'responsable', 'fecha_ingreso', 'fecha_salida', 'modalidad', 'online', 'total_a_pagar'
    fieldsets = (
        (None, {'fields': ['solicitado_a']}),
        (None, {'fields': ('responsable',
            ('fecha_ingreso', 'hora_ingreso_estimada'),
            ('duracion_estadia', 'cantidad_de_personas'), 'modalidad')}),
        (None, {'fields': ('habitaciones', 'observaciones')}))

admin.site.register(Reserva, ReservaAdmin)


class HabitacionAdmin(admin.ModelAdmin):
    list_display = 'numero', 'categoria', 'estado'
    list_filter = 'categoria',

admin.site.register(Habitacion, HabitacionAdmin)


admin.site.register(Modalidad)
admin.site.register(Categoria)
