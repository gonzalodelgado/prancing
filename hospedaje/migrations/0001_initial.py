# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Habitacion'
        db.create_table('hospedaje_habitacion', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('numero', self.gf('django.db.models.fields.PositiveSmallIntegerField')(unique=True, db_index=True)),
            ('max_huespedes', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('tipo', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('hospedaje', ['Habitacion'])


    def backwards(self, orm):
        # Deleting model 'Habitacion'
        db.delete_table('hospedaje_habitacion')


    models = {
        'hospedaje.habitacion': {
            'Meta': {'object_name': 'Habitacion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['hospedaje']