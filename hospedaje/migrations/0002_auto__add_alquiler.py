# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Alquiler'
        db.create_table('hospedaje_alquiler', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_ingreso', self.gf('django.db.models.fields.DateField')()),
            ('hora_ingreso', self.gf('django.db.models.fields.TimeField')(default=datetime.time(9, 0))),
            ('fecha_egreso', self.gf('django.db.models.fields.DateField')()),
            ('hora_egreso', self.gf('django.db.models.fields.TimeField')(default=datetime.time(9, 0))),
        ))
        db.send_create_signal('hospedaje', ['Alquiler'])

        # Adding M2M table for field habitaciones on 'Alquiler'
        db.create_table('hospedaje_alquiler_habitaciones', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('alquiler', models.ForeignKey(orm['hospedaje.alquiler'], null=False)),
            ('habitacion', models.ForeignKey(orm['hospedaje.habitacion'], null=False))
        ))
        db.create_unique('hospedaje_alquiler_habitaciones', ['alquiler_id', 'habitacion_id'])

        # Adding M2M table for field huespedes on 'Alquiler'
        db.create_table('hospedaje_alquiler_huespedes', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('alquiler', models.ForeignKey(orm['hospedaje.alquiler'], null=False)),
            ('huesped', models.ForeignKey(orm['gente.huesped'], null=False))
        ))
        db.create_unique('hospedaje_alquiler_huespedes', ['alquiler_id', 'huesped_id'])


    def backwards(self, orm):
        # Deleting model 'Alquiler'
        db.delete_table('hospedaje_alquiler')

        # Removing M2M table for field habitaciones on 'Alquiler'
        db.delete_table('hospedaje_alquiler_habitaciones')

        # Removing M2M table for field huespedes on 'Alquiler'
        db.delete_table('hospedaje_alquiler_huespedes')


    models = {
        'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gente.Huesped']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        'hospedaje.habitacion': {
            'Meta': {'object_name': 'Habitacion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }

    complete_apps = ['hospedaje']