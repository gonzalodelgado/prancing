# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Modalidad'
        db.create_table('hospedaje_modalidad', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('adicional_tarifa', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        ))
        db.send_create_signal('hospedaje', ['Modalidad'])

        # Adding field 'Alquiler.modalidad'
        db.add_column('hospedaje_alquiler', 'modalidad',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospedaje.Modalidad'], null=True),
                      keep_default=False)

        # Adding field 'Alquiler.observaciones'
        db.add_column('hospedaje_alquiler', 'observaciones',
                      self.gf('django.db.models.fields.TextField')(default='', blank=True),
                      keep_default=False)

        # Adding field 'Habitacion.tarifa_base'
        db.add_column('hospedaje_habitacion', 'tarifa_base',
                      self.gf('django.db.models.fields.DecimalField')(default=100.0, max_digits=15, decimal_places=2),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Modalidad'
        db.delete_table('hospedaje_modalidad')

        # Deleting field 'Alquiler.modalidad'
        db.delete_column('hospedaje_alquiler', 'modalidad_id')

        # Deleting field 'Alquiler.observaciones'
        db.delete_column('hospedaje_alquiler', 'observaciones')

        # Deleting field 'Habitacion.tarifa_base'
        db.delete_column('hospedaje_habitacion', 'tarifa_base')


    models = {
        'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gente.Huesped']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']", 'null': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'hospedaje.habitacion': {
            'Meta': {'object_name': 'Habitacion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'tarifa_base': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'hospedaje.modalidad': {
            'Meta': {'object_name': 'Modalidad'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        }
    }

    complete_apps = ['hospedaje']