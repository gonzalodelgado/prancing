# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Reserva'
        db.create_table('hospedaje_reserva', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_y_hora', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('responsable', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gente.Cliente'])),
            ('cantidad_de_personas', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('duracion_estadia', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('modalidad', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospedaje.Modalidad'])),
        ))
        db.send_create_signal('hospedaje', ['Reserva'])

        # Adding M2M table for field habitaciones on 'Reserva'
        db.create_table('hospedaje_reserva_habitaciones', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('reserva', models.ForeignKey(orm['hospedaje.reserva'], null=False)),
            ('habitacion', models.ForeignKey(orm['hospedaje.habitacion'], null=False))
        ))
        db.create_unique('hospedaje_reserva_habitaciones', ['reserva_id', 'habitacion_id'])


    def backwards(self, orm):
        # Deleting model 'Reserva'
        db.delete_table('hospedaje_reserva')

        # Removing M2M table for field habitaciones on 'Reserva'
        db.delete_table('hospedaje_reserva_habitaciones')


    models = {
        'gente.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gente.Huesped']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']", 'null': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'hospedaje.habitacion': {
            'Meta': {'object_name': 'Habitacion'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'}),
            'tarifa_base': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'tipo': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        'hospedaje.modalidad': {
            'Meta': {'object_name': 'Modalidad'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'hospedaje.reserva': {
            'Meta': {'object_name': 'Reserva'},
            'cantidad_de_personas': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'duracion_estadia': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']"}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']"})
        }
    }

    complete_apps = ['hospedaje']