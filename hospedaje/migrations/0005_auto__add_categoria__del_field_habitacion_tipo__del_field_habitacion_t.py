# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Categoria'
        db.create_table('hospedaje_categoria', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30)),
            ('max_huespedes', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('tarifa_base', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        ))
        db.send_create_signal('hospedaje', ['Categoria'])

        # Deleting field 'Habitacion.tipo'
        db.delete_column('hospedaje_habitacion', 'tipo')

        # Deleting field 'Habitacion.tarifa_base'
        db.delete_column('hospedaje_habitacion', 'tarifa_base')

        # Deleting field 'Habitacion.max_huespedes'
        db.delete_column('hospedaje_habitacion', 'max_huespedes')

        # Adding field 'Habitacion.categoria'
        db.add_column('hospedaje_habitacion', 'categoria',
                      self.gf('django.db.models.fields.related.ForeignKey')(default=0, to=orm['hospedaje.Categoria']),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Categoria'
        db.delete_table('hospedaje_categoria')


        # User chose to not deal with backwards NULL issues for 'Habitacion.tipo'
        raise RuntimeError("Cannot reverse this migration. 'Habitacion.tipo' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Habitacion.tarifa_base'
        raise RuntimeError("Cannot reverse this migration. 'Habitacion.tarifa_base' and its values cannot be restored.")

        # User chose to not deal with backwards NULL issues for 'Habitacion.max_huespedes'
        raise RuntimeError("Cannot reverse this migration. 'Habitacion.max_huespedes' and its values cannot be restored.")
        # Deleting field 'Habitacion.categoria'
        db.delete_column('hospedaje_habitacion', 'categoria_id')


    models = {
        'gente.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gente.Huesped']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']", 'null': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'})
        },
        'hospedaje.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'tarifa_base': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'hospedaje.habitacion': {
            'Meta': {'object_name': 'Habitacion'},
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Categoria']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'})
        },
        'hospedaje.modalidad': {
            'Meta': {'object_name': 'Modalidad'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'hospedaje.reserva': {
            'Meta': {'object_name': 'Reserva'},
            'cantidad_de_personas': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'duracion_estadia': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']"}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']"})
        }
    }

    complete_apps = ['hospedaje']