# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Pago'
        db.create_table('hospedaje_pago', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('titular', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('ultimos_numeros_tarjeta', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('direccion', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('localidad', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('provincia', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
        ))
        db.send_create_signal('hospedaje', ['Pago'])

        # Adding field 'Reserva.pago'
        db.add_column('hospedaje_reserva', 'pago',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospedaje.Pago'], null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting model 'Pago'
        db.delete_table('hospedaje_pago')

        # Deleting field 'Reserva.pago'
        db.delete_column('hospedaje_reserva', 'pago_id')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'gente.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cuit_o_cuil': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'})
        },
        'gente.empleado': {
            'Meta': {'object_name': 'Empleado'},
            'cantidad_de_hijos': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'})
        },
        'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'aprobado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']", 'null': 'True'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']", 'null': 'True', 'blank': 'True'}),
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gente.Huesped']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']", 'null': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'reserva': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hospedaje.Reserva']", 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'hospedaje.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'tarifa_base': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'hospedaje.habitacion': {
            'Meta': {'ordering': "('numero', 'categoria')", 'object_name': 'Habitacion'},
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Categoria']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'})
        },
        'hospedaje.modalidad': {
            'Meta': {'object_name': 'Modalidad'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'hospedaje.pago': {
            'Meta': {'object_name': 'Pago'},
            'direccion': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'titular': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ultimos_numeros_tarjeta': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        'hospedaje.reserva': {
            'Meta': {'object_name': 'Reserva'},
            'cantidad_de_personas': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'duracion_estadia': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_ingreso_estimada': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']"}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'pago': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Pago']", 'null': 'True', 'blank': 'True'}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']"}),
            'solicitado_a': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']", 'null': 'True'})
        },
        'hospedaje.servicio': {
            'Meta': {'object_name': 'Servicio'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'alquiler': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Alquiler']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'solicitado_a': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']", 'null': 'True'})
        }
    }

    complete_apps = ['hospedaje']