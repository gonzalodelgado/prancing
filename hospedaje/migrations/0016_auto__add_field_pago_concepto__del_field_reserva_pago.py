# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Pago.concepto'
        db.add_column(u'hospedaje_pago', 'concepto',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Deleting field 'Reserva.pago'
        db.delete_column(u'hospedaje_reserva', 'pago_id')

        # Adding M2M table for field pagos on 'Reserva'
        m2m_table_name = db.shorten_name(u'hospedaje_reserva_pagos')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('reserva', models.ForeignKey(orm[u'hospedaje.reserva'], null=False)),
            ('pago', models.ForeignKey(orm[u'hospedaje.pago'], null=False))
        ))
        db.create_unique(m2m_table_name, ['reserva_id', 'pago_id'])


    def backwards(self, orm):
        # Deleting field 'Pago.concepto'
        db.delete_column(u'hospedaje_pago', 'concepto')

        # Adding field 'Reserva.pago'
        db.add_column(u'hospedaje_reserva', 'pago',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospedaje.Pago'], null=True, blank=True),
                      keep_default=False)

        # Removing M2M table for field pagos on 'Reserva'
        db.delete_table(db.shorten_name(u'hospedaje_reserva_pagos'))


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'gente.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cuit_o_cuil': ('django.db.models.fields.CharField', [], {'max_length': '15', 'db_index': 'True'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'tipo_cliente': ('django.db.models.fields.CharField', [], {'default': "'particular'", 'max_length': '20'})
        },
        u'gente.empleado': {
            'Meta': {'object_name': 'Empleado'},
            'cantidad_de_hijos': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'})
        },
        u'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'aprobado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gente.Empleado']", 'null': 'True'}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gente.Cliente']", 'null': 'True', 'blank': 'True'}),
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['gente.Huesped']", 'symmetrical': 'False'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospedaje.Modalidad']", 'null': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'reserva': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['hospedaje.Reserva']", 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'hospedaje.categoria': {
            'Meta': {'object_name': 'Categoria'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'tarifa_base': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        u'hospedaje.habitacion': {
            'Meta': {'ordering': "('numero', 'categoria')", 'object_name': 'Habitacion'},
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospedaje.Categoria']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'})
        },
        u'hospedaje.modalidad': {
            'Meta': {'object_name': 'Modalidad'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'hospedaje.pago': {
            'Meta': {'object_name': 'Pago'},
            'concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'titular': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ultimos_numeros_tarjeta': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        },
        u'hospedaje.reserva': {
            'Meta': {'object_name': 'Reserva'},
            'cantidad_de_personas': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'duracion_estadia': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_ingreso_estimada': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)', 'null': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospedaje.Modalidad']"}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'online': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'pagos': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['hospedaje.Pago']", 'null': 'True', 'blank': 'True'}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gente.Cliente']"}),
            'solicitado_a': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gente.Empleado']", 'null': 'True'})
        },
        u'hospedaje.servicio': {
            'Meta': {'object_name': 'Servicio'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'alquiler': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['hospedaje.Alquiler']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50'}),
            'solicitado_a': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['gente.Empleado']", 'null': 'True'})
        }
    }

    complete_apps = ['hospedaje']