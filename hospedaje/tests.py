from datetime import date, timedelta
from django.test import TestCase
from django.contrib.auth.models import User

from gente.models import Cliente, Empleado
from hospedaje.models import Habitacion, Categoria, Reserva, Modalidad, Alquiler


class HabitacionesTestCase(TestCase):

    def setUp(self):
        self.hoy = date.today()
        categoria, creado = Categoria.objects.get_or_create(
            nombre='doble', max_huespedes=2, tarifa_base=100.0)
        if not Habitacion.objects.exists():
            for i in range(1, 11):
                Habitacion.objects.get_or_create(
                    numero=i, categoria=categoria)
        cliente = Cliente.objects.create(
            dni='29323222',
            domicilio='Casa',
            apellido='Doe',
            nombre='John',
            localidad='Aguilares',
            provincia='Tucuman')
        modalidad = Modalidad.objects.create(
            nombre='desayuno', adicional_tarifa=20)

        self.reserva, creado = Reserva.objects.get_or_create(
            fecha_ingreso=self.hoy + timedelta(10),
            duracion_estadia=5,
            cantidad_de_personas=2,
            responsable=cliente,
            modalidad=modalidad)
        if creado:
            self.reserva.habitaciones.add(Habitacion.objects.get(numero=1))
            self.reserva.habitaciones.add(Habitacion.objects.get(numero=2))

        usuario, creado = User.objects.get_or_create(
            first_name='Juan', last_name='Perez', email='juanperez@mail.com',
            username='juan')
        empleado, creado = Empleado.objects.get_or_create(
            user=usuario, cargo='encargado')
        self.alquiler, creado = Alquiler.objects.get_or_create(
            aprobado_por=empleado,
            cliente=cliente,
            fecha_ingreso=self.hoy - timedelta(2),
            fecha_egreso=self.hoy + timedelta(3),
            modalidad=modalidad)
        if creado:
            self.alquiler.habitaciones.add(Habitacion.objects.get(numero=3))
            self.alquiler.habitaciones.add(Habitacion.objects.get(numero=4))

    def test_disponible_antes(self):
        habitacion = self.alquiler.habitaciones.all()[0]
        self.assertEqual(habitacion.estado(
            self.hoy - timedelta(5), self.hoy - timedelta(3)),
            Habitacion.ESTADO_DISPONIBLE)
        self.assertEqual(habitacion.estado(
            self.hoy - timedelta(3), self.hoy),
            Habitacion.ESTADO_OCUPADA)
        self.assertEqual(habitacion.estado(self.hoy, self.hoy + timedelta(2)),
                         Habitacion.ESTADO_OCUPADA)

    def test_disponible_durante(self):
        habitacion = self.reserva.habitaciones.all()[0]
        self.assertEqual(habitacion.estado(
            self.reserva.fecha_ingreso + timedelta(1),
            self.reserva.fecha_ingreso + timedelta(2)),
            Habitacion.ESTADO_RESERVADA)

    def test_disponible_despues(self):
        habitacion = self.reserva.habitaciones.all()[0]
        self.assertEqual(habitacion.estado(
            self.reserva.fecha_ingreso + timedelta(2),
            self.reserva.fecha_ingreso + timedelta(5)),
            Habitacion.ESTADO_RESERVADA)
        self.assertEqual(habitacion.estado(
            self.reserva.fecha_ingreso + timedelta(self.reserva.duracion_estadia + 1),
            self.reserva.fecha_ingreso + timedelta(self.reserva.duracion_estadia + 5)),
            Habitacion.ESTADO_DISPONIBLE)
