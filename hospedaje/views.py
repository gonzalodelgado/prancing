from datetime import timedelta

from django import forms
from django.conf import settings
from django.utils import simplejson
from django.utils.encoding import smart_str
from django.forms.models import model_to_dict, modelform_factory
from django.shortcuts import get_object_or_404
from django.http import HttpResponse, HttpResponseBadRequest

from hospedaje.models import Reserva, Alquiler, Habitacion


def datos_reserva(request, id_reserva):
    if request.is_ajax() or settings.DEBUG:
        reserva = get_object_or_404(Reserva, pk=id_reserva)
        datos = model_to_dict(reserva)
        datos['fecha_ingreso'] = smart_str('%s/%s/%s' % (
            reserva.fecha_ingreso.day,
            reserva.fecha_ingreso.month,
            reserva.fecha_ingreso.year))
        datos['hora_ingreso_estimada'] = smart_str(
            reserva.hora_ingreso_estimada)
        duracion_estadia = timedelta(days=reserva.duracion_estadia)
        fecha_egreso = reserva.fecha_ingreso + duracion_estadia
        datos['fecha_egreso'] = smart_str('%s/%s/%s' % (
            fecha_egreso.day, fecha_egreso.month, fecha_egreso.year))
        return HttpResponse(
            simplejson.dumps(datos), mimetype='application/json')
    return HttpResponseBadRequest()


def ajax_cliente_alquiler(request, id_alquiler):
    if request.is_ajax() or settings.DEBUG:
        alquiler = get_object_or_404(Alquiler, pk=id_alquiler)
        cliente = alquiler.cliente
        if cliente is None:
            cliente = alquiler.reserva.responsable
        id_cliente = simplejson.dumps(cliente.pk)
        return HttpResponse(id_cliente, mimetype='application/json')
    return HttpResponseBadRequest()


class HabitacionesDisponiblesAjaxForm(forms.Form):
    fecha_ingreso = forms.DateField()
    fecha_egreso = forms.DateField(required=False)
    duracion_estadia = forms.IntegerField(required=False)


def habitaciones_disponibles_ajax(request, form_prefix=None):
    if request.is_ajax() or settings.DEBUG:
       form = HabitacionesDisponiblesAjaxForm(request.POST or None, prefix=form_prefix)
       if form.is_valid():
           fecha_desde = form.cleaned_data['fecha_ingreso']
           fecha_hasta = form.cleaned_data.get('fecha_hasta')
           if form.cleaned_data.get('fecha_egreso') is None:
               duracion = timedelta(form.cleaned_data['duracion_estadia'])
               fecha_hasta = fecha_desde + duracion
           habitaciones = simplejson.dumps([{
               'id': h.id, 'numero': h.numero, 'categoria': h.categoria.nombre} for h \
               in Habitacion.objects.all() \
               if h.estado(fecha_desde, fecha_hasta) == h.ESTADO_DISPONIBLE])
           return HttpResponse(habitaciones, mimetype='application/json')
       else:
           print 'ERRORES', form.errors
    return HttpResponseBadRequest()

def categorias_disponibles_ajax(request, form_prefix=None):
    if request.is_ajax() or settings.DEBUG:
       form = HabitacionesDisponiblesAjaxForm(request.POST or None, prefix=form_prefix)
       if form.is_valid():
           fecha_desde = form.cleaned_data['fecha_ingreso']
           fecha_hasta = form.cleaned_data.get('fecha_hasta')
           if form.cleaned_data.get('fecha_egreso') is None:
               duracion = timedelta(form.cleaned_data['duracion_estadia'])
               fecha_hasta = fecha_desde + duracion
           categorias = set(h.categoria for h in Habitacion.objects.all() \
                         if h.estado(fecha_desde, fecha_hasta) == h.ESTADO_DISPONIBLE)
           categorias = simplejson.dumps([{
               'id': categoria.id,
               'nombre': categoria.nombre,
               'tarifa': float(categoria.tarifa_base)} for categoria in categorias])
           return HttpResponse(categorias, mimetype='application/json')
       else:
           print 'ERRORES', form.errors
    return HttpResponseBadRequest()
