from django.contrib import admin

from .models import Pago


class PagoAdmin(admin.ModelAdmin):
    list_display = 'fecha_y_hora', 'titular', 'concepto'

admin.site.register(Pago, PagoAdmin)
