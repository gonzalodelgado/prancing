# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Pago'
        db.create_table(u'pagos_pago', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_y_hora', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('titular', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('ultimos_numeros_tarjeta', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('domicilio', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('localidad', self.gf('django.db.models.fields.CharField')(max_length=200)),
            ('provincia', self.gf('django.db.models.fields.CharField')(max_length=50)),
            ('telefono', self.gf('django.db.models.fields.CharField')(max_length=16, blank=True)),
            ('concepto', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'pagos', ['Pago'])


    def backwards(self, orm):
        # Deleting model 'Pago'
        db.delete_table(u'pagos_pago')


    models = {
        u'pagos.pago': {
            'Meta': {'object_name': 'Pago'},
            'concepto': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'telefono': ('django.db.models.fields.CharField', [], {'max_length': '16', 'blank': 'True'}),
            'titular': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'ultimos_numeros_tarjeta': ('django.db.models.fields.CharField', [], {'max_length': '3'})
        }
    }

    complete_apps = ['pagos']