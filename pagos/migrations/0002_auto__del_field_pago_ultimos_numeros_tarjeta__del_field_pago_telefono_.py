# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Pago.ultimos_numeros_tarjeta'
        db.delete_column(u'pagos_pago', 'ultimos_numeros_tarjeta')

        # Deleting field 'Pago.telefono'
        db.delete_column(u'pagos_pago', 'telefono')

        # Adding field 'Pago.medio_de_pago'
        db.add_column(u'pagos_pago', 'medio_de_pago',
                      self.gf('django.db.models.fields.CharField')(default='efectivo', max_length=20),
                      keep_default=False)

        # Adding field 'Pago.monto'
        db.add_column(u'pagos_pago', 'monto',
                      self.gf('django.db.models.fields.DecimalField')(default=1.0, max_digits=15, decimal_places=2),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Pago.ultimos_numeros_tarjeta'
        db.add_column(u'pagos_pago', 'ultimos_numeros_tarjeta',
                      self.gf('django.db.models.fields.CharField')(default=123, max_length=3),
                      keep_default=False)

        # Adding field 'Pago.telefono'
        db.add_column(u'pagos_pago', 'telefono',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=16, blank=True),
                      keep_default=False)

        # Deleting field 'Pago.medio_de_pago'
        db.delete_column(u'pagos_pago', 'medio_de_pago')

        # Deleting field 'Pago.monto'
        db.delete_column(u'pagos_pago', 'monto')


    models = {
        u'pagos.pago': {
            'Meta': {'object_name': 'Pago'},
            'concepto': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'medio_de_pago': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'monto': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'titular': ('django.db.models.fields.CharField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['pagos']