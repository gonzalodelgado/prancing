# coding: utf-8
from django.db import models
from django.contrib.localflavor.ar.forms import PROVINCE_CHOICES


MEDIOS_DE_PAGO = 'efectivo', 'deposito'

class Pago(models.Model):
    fecha_y_hora = models.DateTimeField(auto_now_add=True, editable=False)
    titular = models.CharField(max_length=200)
    medio_de_pago = models.CharField(
        max_length=20, choices=[(mp, mp) for mp in MEDIOS_DE_PAGO])
    domicilio = models.CharField(max_length=200)
    localidad = models.CharField(max_length=200)
    provincia = models.CharField(max_length=50, choices=PROVINCE_CHOICES)
    monto = models.DecimalField(max_digits=15, decimal_places=2)
    concepto = models.CharField(max_length=100, blank=True)

    concepto_por_defecto = 'gastos varios'

    def save(self, *args, **kwargs):
        self.concepto = self.concepto_por_defecto
        return super(Pago, self).save(*args, **kwargs)
