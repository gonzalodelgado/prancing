from django.contrib.admin import ModelAdmin


class AutoUserFieldAdmin(ModelAdmin):
    user_field = 'user'

    def formfield_for_dbfield(self, dbfield, **kwargs):
        valid_kwargs = 'request' in kwargs and 'initial' not in kwargs
        if dbfield.name == self.user_field and valid_kwargs:
            kwargs['initial'] = kwargs['request'].user.pk
        return super(AutoUserFieldAdmin, self).formfield_for_dbfield(
                dbfield, **kwargs)
