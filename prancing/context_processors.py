from django.conf import settings

def autores(request):
    return {'autores': settings.AUTORES}
