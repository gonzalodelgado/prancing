# -*- coding: utf-8 -*-
"""
This file was generated with the custommenu management command, it contains
the classes for the admin menu, you can customize this class as you want.

To activate your custom menu add the following to your settings.py::
    ADMIN_TOOLS_MENU = 'prancing.menu.CustomMenu'
"""

from django.core.urlresolvers import reverse
from django.utils.translation import ugettext_lazy as _

from admin_tools.menu import items, Menu


class CustomMenu(Menu):
    """
    Custom Menu for prancing admin site.
    """
    def __init__(self, **kwargs):
        Menu.__init__(self, **kwargs)
        self.children += [
            items.MenuItem(_('Inicio'), reverse('admin:index')),
            items.ModelList('Hospedaje', models=(
                'hospedaje.models.Reserva', 'hospedaje.models.Alquiler',
                'hospedaje.Habitaciones', 'gente.models.Cliente',
                'gente.models.Huesped')),
            items.ModelList('Empleados', models=(
                'gente.models.Empleado', 'haberes.models.Liquidacion',
                'haberes.models.Concepto')),
            items.ModelList('Ventas',
                models=('productos.models.Venta', 'productos.models.Producto')),
            items.ModelList('Compras', models=('productos.models.Compra',
                                               'productos.models.Insumo',
                                               'gente.models.Proveedor'))]

    def init_with_context(self, context):
        """
        Use this method if you need to access the request context.
        """
        request = context['request']
        if request.user.is_superuser:
            self.children.append(items.ModelList(u'Usuarios', models=(
                'django.contrib.auth.*', 'gente.models.RegistroDeActividad')))
        return super(CustomMenu, self).init_with_context(context)
