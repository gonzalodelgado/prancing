from django.conf import settings
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import DetailView, ListView

from productos.models import Venta
from haberes.models import Liquidacion
from gente.models import Huesped


admin.autodiscover()


urlpatterns = patterns('',
    url(r'^admin_tools/', include('admin_tools.urls')),
    url(r'^productos/producto/(?P<codigo_producto>\d+)/ajax/precio_unitario/$',
        'productos.views.ajax_precio_unitario',
        name='productos_ajax_precio_unitario'),
    url(r'^productos/insumo/(?P<codigo_insumo>\d+)/ajax/precio_de_costo/$',
        'productos.views.ajax_precio_de_costo',
        name='productos_ajax_precio_de_costo'),
    url(r'^hospedaje/reserva/(?P<id_reserva>\d+)/ajax/$',
        'hospedaje.views.datos_reserva', name='hospedaje_reserva_ajax'),
    url(r'^hospedaje/alquiler/(?P<id_alquiler>\d+)/ajax/cliente/$',
        'hospedaje.views.ajax_cliente_alquiler',
        name='hospedaje_cliente_alquiler_ajax'),
    url(r'^hospedaje/habitaciones/ajax/disponibles(?:/(?P<form_prefix>[\w\-_]+))?/$',
        'hospedaje.views.habitaciones_disponibles_ajax',
        name='hospedaje_habitaciones_disponibles_ajax'),
    url(r'^hospedaje/categorias/ajax/disponibles(?:/(?P<form_prefix>[\w\-_]+))?/$',
        'hospedaje.views.categorias_disponibles_ajax',
        name='hospedaje_categorias_disponibles_ajax'),
    url(r'^productos/venta/(?P<pk>\d+)/imprimir/$', DetailView.as_view(
        template_name='imprimir_venta.html', model=Venta,
        context_object_name='venta'), name='productos_venta_imprimir'),
    url(r'^haberes/liquidacion/(?P<pk>\d+)/imprimir/$', DetailView.as_view(
        template_name='imprimir_liquidacion.html', model=Liquidacion,
        context_object_name='liquidacion'), name='haberes_liquidacion_imprimir'),
    url(r'^gente/huesped/imprimir/$', ListView.as_view(
        template_name='imprimir_huesped.html', model=Huesped,
        context_object_name='huespedes'), name='hospedaje_huesped_imprimir'),
    url(r'^administracion/', include(admin.site.urls)),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
