from django.contrib import admin
from reversion import VersionAdmin

from prancing.admin import AutoUserFieldAdmin
from .forms import InsumoForm
from .models import (
    Producto, Venta, Insumo, LineaDeVenta, LineaCompra, Compra)


CANTIDAD_DE_LINEAS = 5


class LineaDeVentaInline(admin.TabularInline):
    model = LineaDeVenta
    extra = CANTIDAD_DE_LINEAS


class VentaAdmin(AutoUserFieldAdmin):
    user_field = 'vendedor'
    list_display = 'fecha_y_hora', 'cliente', 'alquiler', 'vendedor'
    inlines = [LineaDeVentaInline]

admin.site.register(Venta, VentaAdmin)


class ProductoAdmin(VersionAdmin):
    list_display = 'codigo', 'nombre', 'stock', 'precio_unitario'

admin.site.register(Producto, ProductoAdmin)


class InsumoAdmin(VersionAdmin):
    form = InsumoForm
    list_display = 'codigo', 'nombre', 'stock', 'precio_de_costo'

admin.site.register(Insumo, InsumoAdmin)


class LineaCompraInline(admin.TabularInline):
    model = LineaCompra
    extra = CANTIDAD_DE_LINEAS


class CompraAdmin(admin.ModelAdmin):
    list_display = 'fecha_y_hora', 'condicion_pago', 'proveedor'
    inlines = [LineaCompraInline]

admin.site.register(Compra, CompraAdmin)
