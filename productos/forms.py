from django import forms

from .models import Insumo


class InsumoForm(forms.ModelForm):
    class Meta:
        model = Insumo

    def clean(self):
        data = super(InsumoForm, self).clean()
        precio_unitario = data.get('precio_unitario')
        precio_de_costo = data.get('precio_de_costo')
        if precio_unitario and precio_de_costo and precio_unitario < precio_de_costo:
            self._errors['precio_unitario'] = self.error_class(['El precio de venta debe ser mayor al de compra'])
            self._errors['precio_de_costo'] = self.error_class(['El precio de compra debe ser menor al de venta'])
            del data['precio_unitario']
            del data['precio_de_costo']
        return data
