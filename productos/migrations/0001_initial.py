# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Producto'
        db.create_table('productos_producto', (
            ('codigo', self.gf('django.db.models.fields.SlugField')(max_length=50, primary_key=True)),
            ('nombre', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('descripcion', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('stock', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('precio_unitario', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
        ))
        db.send_create_signal('productos', ['Producto'])

        # Adding model 'Venta'
        db.create_table('productos_venta', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('fecha_y_hora', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('importe', self.gf('django.db.models.fields.DecimalField')(max_digits=15, decimal_places=2)),
            ('cliente', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gente.Cliente'])),
            ('alquiler', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['hospedaje.Alquiler'])),
            ('vendedor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['gente.Empleado'])),
        ))
        db.send_create_signal('productos', ['Venta'])

        # Adding model 'LineaDeVenta'
        db.create_table('productos_lineadeventa', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('venta', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['productos.Venta'])),
            ('producto', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['productos.Producto'])),
            ('cantidad', self.gf('django.db.models.fields.PositiveSmallIntegerField')()),
            ('descuento', self.gf('django.db.models.fields.DecimalField')(max_digits=6, decimal_places=2)),
        ))
        db.send_create_signal('productos', ['LineaDeVenta'])


    def backwards(self, orm):
        # Deleting model 'Producto'
        db.delete_table('productos_producto')

        # Deleting model 'Venta'
        db.delete_table('productos_venta')

        # Deleting model 'LineaDeVenta'
        db.delete_table('productos_lineadeventa')


    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'gente.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cuit_o_cuil': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'gente.empleado': {
            'Meta': {'object_name': 'Empleado'},
            'cantidad_de_hijos': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'aprovado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']", 'null': 'True'}),
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gente.Huesped']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']", 'null': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'reserva': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hospedaje.Reserva']", 'unique': 'True', 'null': 'True'})
        },
        'hospedaje.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'tarifa_base': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'hospedaje.habitacion': {
            'Meta': {'object_name': 'Habitacion'},
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Categoria']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'})
        },
        'hospedaje.modalidad': {
            'Meta': {'object_name': 'Modalidad'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'hospedaje.reserva': {
            'Meta': {'object_name': 'Reserva'},
            'cantidad_de_personas': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'duracion_estadia': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_ingreso_estimada': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']"}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']"}),
            'solicitado_a': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']", 'null': 'True'})
        },
        'productos.lineadeventa': {
            'Meta': {'object_name': 'LineaDeVenta'},
            'cantidad': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'descuento': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['productos.Producto']"}),
            'venta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['productos.Venta']"})
        },
        'productos.producto': {
            'Meta': {'object_name': 'Producto'},
            'codigo': ('django.db.models.fields.SlugField', [], {'max_length': '50', 'primary_key': 'True'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'precio_unitario': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'stock': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'productos.venta': {
            'Meta': {'object_name': 'Venta'},
            'alquiler': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Alquiler']"}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']"}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']"})
        }
    }

    complete_apps = ['productos']