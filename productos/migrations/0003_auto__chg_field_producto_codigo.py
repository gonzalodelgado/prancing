# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Producto.codigo'
        db.alter_column('productos_producto', 'codigo', self.gf('django.db.models.fields.AutoField')(primary_key=True))

    def backwards(self, orm):

        # Changing field 'Producto.codigo'
        db.alter_column('productos_producto', 'codigo', self.gf('django.db.models.fields.SlugField')(max_length=50, primary_key=True))

    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'ordering': "('content_type__app_label', 'content_type__model', 'codename')", 'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'gente.cliente': {
            'Meta': {'object_name': 'Cliente'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cuit_o_cuil': ('django.db.models.fields.CharField', [], {'max_length': '15'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'gente.empleado': {
            'Meta': {'object_name': 'Empleado'},
            'cantidad_de_hijos': ('django.db.models.fields.PositiveSmallIntegerField', [], {'default': '0'}),
            'cargo': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['auth.User']"})
        },
        'gente.huesped': {
            'Meta': {'object_name': 'Huesped'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'gente.proveedor': {
            'Meta': {'object_name': 'Proveedor'},
            'apellido': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'cuit': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '15', 'db_index': 'True'}),
            'dni': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'domicilio': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'localidad': ('django.db.models.fields.CharField', [], {'max_length': '200'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'provincia': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'hospedaje.alquiler': {
            'Meta': {'object_name': 'Alquiler'},
            'aprobado_por': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']", 'null': 'True'}),
            'fecha_egreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_egreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'hora_ingreso': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)'}),
            'huespedes': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['gente.Huesped']", 'symmetrical': 'False'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']", 'null': 'True'}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'reserva': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['hospedaje.Reserva']", 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        'hospedaje.categoria': {
            'Meta': {'object_name': 'Categoria'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_huespedes': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'nombre': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'}),
            'tarifa_base': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'hospedaje.habitacion': {
            'Meta': {'ordering': "('numero', 'categoria')", 'object_name': 'Habitacion'},
            'categoria': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Categoria']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'numero': ('django.db.models.fields.PositiveSmallIntegerField', [], {'unique': 'True', 'db_index': 'True'})
        },
        'hospedaje.modalidad': {
            'Meta': {'object_name': 'Modalidad'},
            'adicional_tarifa': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        'hospedaje.reserva': {
            'Meta': {'object_name': 'Reserva'},
            'cantidad_de_personas': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'duracion_estadia': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'fecha_ingreso': ('django.db.models.fields.DateField', [], {}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'habitaciones': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['hospedaje.Habitacion']", 'symmetrical': 'False'}),
            'hora_ingreso_estimada': ('django.db.models.fields.TimeField', [], {'default': 'datetime.time(9, 0)', 'null': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modalidad': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Modalidad']"}),
            'observaciones': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'responsable': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']"}),
            'solicitado_a': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']", 'null': 'True'})
        },
        'productos.compra': {
            'Meta': {'object_name': 'Compra'},
            'condicion_pago': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'proveedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Proveedor']"}),
            'total': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'})
        },
        'productos.insumo': {
            'Meta': {'object_name': 'Insumo', '_ormbases': ['productos.Producto']},
            'precio_de_costo': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'producto_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['productos.Producto']", 'unique': 'True', 'primary_key': 'True'})
        },
        'productos.lineacompra': {
            'Meta': {'object_name': 'LineaCompra'},
            'cantidad': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'compra': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['productos.Compra']"}),
            'descuento': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'insumo': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['productos.Insumo']"})
        },
        'productos.lineadeventa': {
            'Meta': {'object_name': 'LineaDeVenta'},
            'cantidad': ('django.db.models.fields.PositiveSmallIntegerField', [], {}),
            'descuento': ('django.db.models.fields.DecimalField', [], {'max_digits': '6', 'decimal_places': '2'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'producto': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['productos.Producto']"}),
            'venta': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['productos.Venta']"})
        },
        'productos.producto': {
            'Meta': {'object_name': 'Producto'},
            'codigo': ('django.db.models.fields.AutoField', [], {'primary_key': 'True', 'db_index': 'True'}),
            'descripcion': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'nombre': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'precio_unitario': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'stock': ('django.db.models.fields.PositiveSmallIntegerField', [], {})
        },
        'productos.venta': {
            'Meta': {'object_name': 'Venta'},
            'alquiler': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['hospedaje.Alquiler']"}),
            'cliente': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Cliente']"}),
            'fecha_y_hora': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'importe': ('django.db.models.fields.DecimalField', [], {'max_digits': '15', 'decimal_places': '2'}),
            'vendedor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['gente.Empleado']"})
        }
    }

    complete_apps = ['productos']