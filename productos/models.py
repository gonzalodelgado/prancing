# -*- coding: utf-8 -*-
from decimal import Decimal
from django.db import models


ESTADO_ABIERTO = 'abierto'
ESTADO_CERRADO = 'cerrado'


class Producto(models.Model):
    codigo = models.AutoField(u'Código',
        db_index=True, primary_key=True, editable=False)
    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(u'Descripción', blank=True)
    stock = models.PositiveSmallIntegerField(u'Existencia')
    precio_unitario = models.DecimalField(max_digits=15, decimal_places=2)

    def __unicode__(self):
        return u'%s ($%s c/u)' % (self.nombre, self.precio_unitario)


class Venta(models.Model):
    fecha_y_hora = models.DateTimeField(auto_now_add=True)
    alquiler = models.ForeignKey('hospedaje.Alquiler', null=True, blank=True)
    cliente = models.ForeignKey('gente.Cliente', null=True, blank=True)
    vendedor = models.ForeignKey('gente.Empleado')
    total = models.DecimalField(editable=False,
        max_digits=15, decimal_places=2, default=0.0)
    estado = models.CharField(
        max_length=10, editable=False, default=ESTADO_ABIERTO)

    def __unicode__(self):
        cliente = self.cliente or self.alquiler
        return u'Venta a %s por $%s' % (
            cliente, self.total or self.calcular_total())

    def calcular_total(self):
        return sum(
            v['subtotal'] for v in self.lineadeventa_set.values('subtotal'))

    def save(self, **kwargs):
        super(Venta, self).save(**kwargs)
        if not self.total:
            self.total = self.calcular_total()
        return super(Venta, self).save(**kwargs)


class LineaDeVenta(models.Model):
    venta = models.ForeignKey(Venta)
    producto = models.ForeignKey(Producto, null=True, on_delete=models.SET_NULL)
    cantidad = models.PositiveSmallIntegerField(default=1)
    descuento = models.DecimalField(u'Descuento (%)',
        max_digits=6, decimal_places=2, default=0.0, blank=True)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2, default=0.0)

    class Meta:
        verbose_name = u'Línea de venta'
        verbose_name_plural = u'Líneas de venta'

    def __unicode__(self):
        subtotal = self.cantidad * self.producto.precio_unitario
        if self.descuento:
            subtotal = subtotal * (self.descuento / Decimal(100.0))
        return u'Subtotal: $%s' % subtotal

    def save(self, **kwargs):
        linea = super(LineaDeVenta, self).save(**kwargs)
        self.producto.stock -= self.cantidad
        return linea

    def delete(self, **kwargs):
        self.producto.stock += self.cantidad
        return super(LineaDeVenta, self).delete(**kwargs)


class Insumo(Producto):
    precio_de_costo = models.DecimalField(max_digits=15, decimal_places=2)

    def __unicode__(self):
        return u'%s ($%s c/u)' % (self.nombre, self.precio_de_costo)


OPCIONES_PAGO = (('efectivo', u'Efectivo'),)


class Compra(models.Model):
    fecha_y_hora = models.DateTimeField(auto_now_add=True)
    condicion_pago = models.CharField(
        max_length=10, default='efectivo', choices=OPCIONES_PAGO)
    proveedor = models.ForeignKey('gente.Proveedor')
    total = models.DecimalField(editable=False,
        max_digits=15, decimal_places=2, default=0.0)
    estado = models.CharField(
        max_length=10, editable=False, default=ESTADO_ABIERTO)

    def __unicode__(self):
        total = self.total or self.calcular_total()
        return u'Compra a %s por $%s' % (self.proveedor, total)

    def calcular_total(self):
        return sum(
            v['subtotal'] for v in self.lineacompra_set.values('subtotal'))

    def save(self, **kwargs):
        super(Compra, self).save(**kwargs)
        if not self.total:
            self.total = self.calcular_total()
        return super(Compra, self).save(**kwargs)


class LineaCompra(models.Model):
    insumo = models.ForeignKey(Insumo, null=True, on_delete=models.SET_NULL)
    cantidad = models.PositiveSmallIntegerField(default=1)
    descuento = models.DecimalField(u'Descuento (%)',
        max_digits=6, decimal_places=2, default=0.0, blank=True)
    compra = models.ForeignKey(Compra)
    subtotal = models.DecimalField(max_digits=15, decimal_places=2, default=0.0)

    class Meta:
        verbose_name = u'Línea de compra'
        verbose_name_plural = u'Líneas de compra'

    def __unicode__(self):
        subtotal = self.cantidad * self.insumo.precio_de_costo
        if self.descuento:
            subtotal = subtotal * (self.descuento / Decimal(100.0))
        return u'Subtotal: $%s' % subtotal

    def save(self, **kwargs):
        linea = super(LineaCompra, self).save(**kwargs)
        self.insumo.stock += self.cantidad
        return linea

    def delete(self, **kwargs):
        self.insumo.stock -= self.cantidad
        return super(LineaCompra, self).delete(**kwargs)
