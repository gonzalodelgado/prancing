from django.conf import settings
from django.utils import simplejson
from django.http import HttpResponse, HttpResponseBadRequest
from django.shortcuts import get_object_or_404

from productos.models import Producto, Insumo

def ajax_precio_unitario(request, codigo_producto):
    if request.is_ajax() or settings.DEBUG:
        producto = get_object_or_404(Producto, codigo=codigo_producto)
        precio_unitario = float(producto.precio_unitario)
        return HttpResponse(simplejson.dumps(precio_unitario),
                mimetype='application/json')
    return HttpResponseBadRequest()


def ajax_precio_de_costo(request, codigo_insumo):
    if request.is_ajax() or settings.DEBUG:
        insumo = get_object_or_404(Insumo, codigo=codigo_insumo)
        precio_de_costo = float(insumo.precio_de_costo)
        return HttpResponse(simplejson.dumps(precio_de_costo),
                mimetype='application/json')
    return HttpResponseBadRequest()
