# coding: utf-8
import re
from datetime import date

import floppyforms as forms
from django.forms import ModelForm, ModelChoiceField, ModelMultipleChoiceField, ValidationError
from django.contrib.localflavor.ar.forms import ARDNIField

from gente.models import Cliente
from hospedaje.models import Categoria, Modalidad
from .widgets import CategoriaCheckboxSelectMultiple


class ReservaForm(forms.Form):
    fecha_ingreso = forms.DateField(label='Fecha de ingreso')
    cantidad_de_personas = forms.IntegerField(initial=1, min_value=1)
    duracion_estadia = forms.IntegerField(initial=1, min_value=1)
    modalidad = ModelChoiceField(queryset=Modalidad.objects.all())
    habitaciones = ModelMultipleChoiceField(widget=CategoriaCheckboxSelectMultiple,
                                            queryset=Categoria.objects.all())

    def clean(self):
        cleaned_data = super(ReservaForm, self).clean()
        cantidad_de_personas = cleaned_data.get('cantidad_de_personas')
        categorias = cleaned_data.get('habitaciones')
        if cantidad_de_personas and categorias:
            disponibilidad = sum(c.max_huespedes for c in categorias)
            if disponibilidad < cantidad_de_personas:
                raise ValidationError(
                    'Por favor seleccione mas habitaciones para poder alojar la cantidad de personas')
        return cleaned_data

class ClienteForm(ModelForm):
    dni = ARDNIField()

    class Meta:
        model = Cliente
        fields = ('dni', 'nombre', 'apellido', 'email', 'telefono',
                  'domicilio', 'localidad', 'provincia')
        exclude = 'cuit_o_cuil'


credit_card_regex = re.compile(r'|'.join([
    '^(?:4[0-9]{12}?:[0-9]{3})?',  # VISA
    '5[1-5][0-9]{14}',  # MasterCard
    '3[47][0-9]{13}',   # American Express
    '3(?:0[0-5]|[68][0-9])[0-9]{11}',   # Diners Club
]))
ANIO = date.today().year

class TarjetaDeCreditoForm(forms.Form):
    titular = forms.CharField(max_length=200)
    numero = forms.RegexField(regex=credit_card_regex)
    caduca_mes = forms.ChoiceField(label='Mes Vencimiento', choices=enumerate([
        'Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio',
        'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'], 1))
    caduca_anio = forms.ChoiceField(label=u'AÃ±o Vencimiento', choices=((anio, anio) for anio in xrange(ANIO, ANIO + 6)))
