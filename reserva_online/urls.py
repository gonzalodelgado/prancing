from django.conf.urls import patterns, url

urlpatterns = patterns('reserva_online.views',
    url(r'^$', 'reserva_online', name='reserva_online'),
    url(r'^(?P<pk>\d+)/$', 'detalle_reserva', name='detalle_reserva'),
)
