from datetime import timedelta

from django.views.generic import DetailView
from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.formtools.wizard.views import SessionWizardView

from hospedaje.models import Reserva, Pago
from .forms import ReservaForm, ClienteForm, TarjetaDeCreditoForm


class DetalleReservaView(DetailView):
    model = Reserva
    queryset = Reserva.objects.filter(online=True)
    template_name = 'reserva_online/detalle.html'

detalle_reserva = DetalleReservaView.as_view()


class ReservaOnlineView(SessionWizardView):
    def done(self, form_list, **kwargs):
        reserva_form, cliente_form, tarjeta_form = form_list
        # reserva_form no es un modelform, creamos reserva a mano
        cliente = cliente_form.save()
        # buscamos habitaciones segun las categorias elegidas
        duracion_estadia = timedelta(days=reserva_form.cleaned_data['duracion_estadia'])
        fecha_ingreso = reserva_form.cleaned_data['fecha_ingreso']
        fecha_egreso = fecha_ingreso + duracion_estadia
        habitaciones = []
        for categoria in reserva_form.cleaned_data['habitaciones']:
            candidatas = filter(lambda h: h.estado(fecha_ingreso, fecha_egreso), categoria.habitacion_set.all())
            if candidatas:
                habitaciones.append(candidatas[0])
        pago = Pago.objects.create(
            titular=tarjeta_form.cleaned_data['titular'],
            ultimos_numeros_tarjeta=tarjeta_form['numero'][:-3],
            domicilio=cliente.domicilio,
            localidad=cliente.localidad,
            provincia=cliente.provincia,
            telefono=cliente.telefono)
        reserva = Reserva.objects.create(
            responsable=cliente,
            pago=pago,
            fecha_ingreso=fecha_ingreso,
            duracion_estadia=duracion_estadia.days,
            cantidad_de_personas=reserva_form.cleaned_data['cantidad_de_personas'],
            modalidad=reserva_form.cleaned_data['modalidad'],
            online=True)
        reserva.habitaciones.add(*habitaciones)
        return HttpResponseRedirect(reverse(detalle_reserva, args=(reserva.id,)))

reserva_online = ReservaOnlineView.as_view([ReservaForm, ClienteForm, TarjetaDeCreditoForm])
