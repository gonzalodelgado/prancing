import floppyforms as forms


class CategoriaCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    template_name = 'floppyforms/simple_checkbox_select.html'
